#include "util.h"
#include "mem.h"
#include "mem_internals.h"
#include <stdbool.h>

static struct block_header* heap;
static size_t shift = 17;

static void assert_block_count(size_t expected) {
    size_t count = 0;
    for (struct block_header* current = heap; current != NULL; current = current->next) {
        count++;
    }

    assert_that(count == expected, "wrong number of blocks; expected %d, actual %d", expected, count);
}

struct block_header* get_block(size_t index) {
    struct block_header* current = heap;
    
    for (size_t i = 0; i < index; i++) {
        if (current == NULL) {
            err("block #%d does not exist", i);
        }

        current = current->next;
    }
    
    return current;
}

void test_heap_init() {
    if (get_block(0) != NULL) {
        err("should be no heap before initialization");
    }

    size_t heap_size = 8192;
    debug("initializing heap with size %d\n", heap_size);
    heap = heap_init(heap_size);
    if (heap == NULL) {
        err("failed to initialize heap\n");
    }

    assert_block_count(1);
    struct block_header* block_header = get_block(0);
    assert_that(block_header == heap, "first block of heap should have the same address as heap");

    debug_heap(stdout, heap);
}

void test_single_alloc_and_free() {
    size_t size = 500;
    debug("allocating memory of %d bytes\n", size);
    int* addr = _malloc(size);
    if (addr == NULL) {
        err("failed to allocate memory");
    }
    assert_block_count(2);
    struct block_header* block = get_block(0);
    assert_that(block->capacity.bytes == size, "wrong block size");

    debug_heap(stdout, heap);

    debug("freeing allocated memory\n");
    _free(addr);
    assert_block_count(1);
    debug_heap(stdout, heap);
}

void test_two_alloc_and_free() {
    size_t size = 512;
    debug("allocating 2 chunks of memory of %d bytes each\n", size);
    int* addr1 = _malloc(size);
    int* addr2 = _malloc(size);
    if (addr1 == NULL || addr2 == NULL) {
        err("failed to allocate memory");
    }
    debug_heap(stdout, heap);
    assert_block_count(3);

    debug("freeing second chunk\n");
    _free(addr2);
    debug_heap(stdout, heap);
    assert_block_count(2);

    debug("freeing first chunk\n");
    _free(addr1);
    debug_heap(stdout, heap);
    assert_block_count(1);
}

void test_heap_grow() {
    size_t size = 10000;
    assert_that(heap->capacity.bytes < size, "%d fits in current heap capacity (%d)", size, heap->capacity.bytes);

    debug("allocating %d bytes\n", size);
    void* addr = _malloc(size);
    if (addr == NULL) {
        err("failed to allocate memory");
    }
    debug_heap(stdout, heap);

    debug("free allocated memory\n");
    _free(addr);
    debug_heap(stdout, heap);

    assert_block_count(1);
    assert_that(heap->is_free, "first block should be free");
}

void test_heap_another_place_allocation() {
    assert_block_count(1);

    debug("allocating new heap\n");
    void *current_heap_end = (uint8_t *) heap + heap->capacity.bytes + shift;
    void *new_heap = map_pages((uint8_t * )(round_pages((size_t) current_heap_end)), 10000, 0);
    debug("allocating new address\n");
    void *addr = _malloc(20000);

    assert_that(addr != current_heap_end, "heap was allocated in wrong address");
    debug_heap(stdout, heap);
    assert_block_count(2);
    _free(addr);
    debug_heap(stdout, heap);
    assert_block_count(1);

    assert_that(heap->is_free, "initial block marked as free");
    _free(new_heap);
}

int main() {
    test_heap_init();
    test_single_alloc_and_free();
    test_two_alloc_and_free();
    test_heap_grow();
    test_heap_another_place_allocation();
}